<template>
  <v-form
    ref="form"
    v-model="validForm"
    light
    :class="!mobile ? 'px-4 mt-4' : ''"
  >
    <v-row class="black--text">
      <!--
        All fields:
        NOTE: this is a combination of custom fields and default fields. Default fields can also have a custom field
        definition, so this has to account for that
      -->
      <v-col v-for="fieldDef in filteredFieldDefs" :key="fieldDef.label"
        :cols="fieldCols(fieldDef)"
        class="pa-1"
      >
        <!-- default fields -->
        <CustomField
          v-if="isDefaultField(fieldDef)"
          :fieldDef="fieldDef"
          :fieldValue.sync="formData[getFieldKey(fieldDef)]"
          :submitted="submitted"
        />

        <!-- custom fields -->
        <CustomField
          v-else
          :fieldDef="fieldDef"
          :fieldValue.sync="formData.customFields[getFieldKey(fieldDef)]"
        />
      </v-col>
    </v-row>

    <v-row>
      <!-- Joining questions -->
      <v-col v-if="answers && answers.length" :class="mobile ? 'pt-4 px-0' : 'pt-6 px-0'">
        <p class="overline pa-4 pb-0">Please answer the following questions</p>
        <v-col cols="12" sm="12" v-for="(question, i) in answers" :key="`j-q-${i}`" class="pa-1">
          <v-text-field
            :class="questionIndent(answers[i].label)"
            v-model="answers[i].value"
            v-bind="customProps"
            :label="answers[i].label"
          />
        </v-col>
      </v-col>

      <!-- TODO: this is specific to WPH so we should disable it for other tribes -->
      <v-col cols="12">
        <v-checkbox
          v-model="declaration"
          label="Please agree to the terms and Conditions*"
          hide-details="auto"
          v-bind="customProps"
          :rules="[v => (Boolean(v)) || 'required field']"
          class="pt-4"
        />
        <ul class="declaration pl-10 pt-2">
          <li class="py-2">
            I agree to the Whangaroa Papa Hapū (Whangaroa PH) retaining a copy of this form for the specific purpose of establishing a Whangaroa Māori population register to take Whangaroa through the Waitangi Tribunal Reparation (Settlement) Process.
          </li>
          <li class="py-2">
            I expect regular notification of progress from the Whangaroa PH;
          </li>
          <li class="py-2">
            I will contact the (hapū) should my address or any other details change in the future.
          </li>
        </ul>
      </v-col>
    </v-row>
  </v-form>
</template>

<script>
import { getCustomFields, getDefaultFields, mapLabelToProp, getDefaultFieldValue } from '@/lib/custom-field-helpers'

import CustomField from '@/components/profile/CustomField.vue'

export default {
  name: 'RegistrationForm',
  props: {
    tribe: {
      type: Object,
      required: true
    },
    questions: Array,
    valid: Boolean
  },
  components: {
    CustomField
  },
  data () {
    return {
      validForm: this.valid,
      formData: {
        customFields: {}
      },
      answers: [],
      declaration: false,
      customProps: {
        hideDetails: 'auto',
        placeholder: ' ',
        light: true,
        outlined: true
      },
      submitted: false
    }
  },
  mounted () {
    this.populateFieldValues()
  },
  watch: {
    valid (valid) {
      this.validForm = valid
    },
    validForm (valid) {
      this.$emit('update:valid', valid)
    },
    questions: {
      immediate: true,
      handler (newVal) {
        this.answers = newVal
      }
    },
    answers: {
      immediate: true,
      deep: true,
      handler (newVal) {
        this.$emit('update:answers', newVal)
      }
    },
    formData: {
      deep: true,
      handler (formData) {
        this.$emit('change', formData)
      }
    }
  },
  computed: {
    mobile () {
      return this.$vuetify.breakpoint.xs || this.$vuetify.breakpoint.sm
    },

    // these are custom field definitions that are defined on the community
    rawCustomFieldDefs () {
      return this.tribe?.customFields || []
    },

    allFields () {
      return [...this.defaultFieldDefs, ...this.customFieldDefs]
        // TODO 2024-02-13: dont have support for images, so we are filtering it out for now
        .filter(fieldDef => fieldDef.type !== 'image')
    },

    fieldDefsWithOrders () {
      return this.allFields
        .filter(field => !field.tombstone && field.order !== null)
        .sort((a, b) => a.order - b.order)
    },

    // Computed property for fields without orders
    fieldDefsWithoutOrders () {
      return this.allFields
        .filter(field => !field.tombstone && field.order === null)
    },

    // Computed property to combine the two lists
    filteredFieldDefs () {
      return [
        ...this.fieldDefsWithOrders,
        ...this.fieldDefsWithoutOrders
      ]
    },

    // custom fields that have been defined
    customFieldDefs () {
      return getCustomFields(this.rawCustomFieldDefs)
    },

    // default fields (both customised and uncustomised default fields)
    defaultFieldDefs () {
      return getDefaultFields(this.rawCustomFieldDefs)
    },

    // default fields which have been customised
    customDefaultFieldDefs () {
      return this.defaultFieldDefs.filter(field => field.key)
    }
  },
  methods: {
    validate () {
      this.submitted = true
      this.$refs.form.validate()
    },
    populateFieldValues () {
      this.allFields.forEach(fieldDef => {
        if (this.isDefaultField(fieldDef)) {
          const prop = mapLabelToProp(fieldDef.label)
          this.formData[prop] = getDefaultFieldValue(fieldDef)
          return
        }

        this.formData.customFields[fieldDef.key] = getDefaultFieldValue(fieldDef)
      })
    },
    fieldCols (fieldDef) {
      if (this.mobile) return '12'

      // force arrays to always be on their own line
      if (fieldDef.type === 'array') return '12'

      // force checkboxes to always be on their own line
      if (fieldDef.type === 'checkbox') return '12'

      return fieldDef.cols || '6'
    },
    isDefaultField (fieldDef) {
      return this.defaultFieldDefs.find(def => def.label === fieldDef.label)
    },
    getFieldKey (fieldDef) {
      // if the field is a default field, we use the property key
      return this.isDefaultField(fieldDef)
        ? mapLabelToProp(fieldDef.label)
        : fieldDef.key
    },
    questionIndent (label = '') {
      const count = label.match(/(great|parent|mother|maternal|father|paternal)/ig)?.length
      switch (count) {
        case 2: return 'ml-8'
        case 3: return 'ml-16'
        default: return ''
      }
    }
  }
}
</script>

<style scoped lang="scss">
.declaration {
    color:  rgba(0, 0, 0, 0.6);
  }
</style>
