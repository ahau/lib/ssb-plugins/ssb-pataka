export function setDefaultData (withRelationships) {
  const formData = {
    type: 'person',
    id: '',
    preferredName: '',
    legalName: '',
    altNames: {
      add: []
    },
    gender: '',
    legallyAdopted: false,
    children: [],
    avatarImage: {},
    aliveInterval: '',
    birthOrder: '',
    description: '',
    city: '',
    country: '',
    postCode: '',
    profession: '',
    address: '',
    email: '',
    phone: '',
    deceased: false,
    placeOfBirth: '',
    placeOfDeath: '',
    buriedLocation: '',
    education: [],
    school: [],
    customFields: {}
  }

  if (!withRelationships) {
    delete formData.relationshipType
    delete formData.legallyAdopted
  }

  return formData
}

const validEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const validFullName = /\s/
const validPhone = /^([^a-zA-Z]*)$/

export const RULES = {
  legalName: [
    v => (v && validFullName.test(v)) || 'Please write your full name, including first name. middles names and last names'
  ],
  email: [
    v => (v && validEmail.test(v)) || 'Please enter a valid email'
  ],
  phone: [
    v => (v && validPhone.test(v)) || 'Please enter a valid phone number'
  ]
}

export const ignoreList = new Set([
  'status'
])
