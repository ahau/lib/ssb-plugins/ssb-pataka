module.exports = function hackURL (url) {
  if (!url) return

  const u = new URL(url)
  u.protocol = window.location.protocol
  u.hostname = window.location.hostname
  u.port = process.env.NODE_ENV === 'development' ? 28068 : ''

  return u.href
}
// HACK 2021-08-09 - ideally the backend knows the correct host
// currently it assumes localhost (see also ssb-serve-blobs/id-to-url)
//
// UPDATE 2024-03-21 - actually lets keep this hack, sometimes it's
// legitimate to be accessing this via localhost!
