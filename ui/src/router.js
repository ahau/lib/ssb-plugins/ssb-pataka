import Vue from 'vue'
import Router from 'vue-router'

import RegistrationIndex from '@/views/RegistrationIndex.vue'
import RegistrationNew from '@/views/RegistrationNew.vue'
import RegistrationSuccess from '@/views/RegistrationSuccess.vue'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    { path: '/', redirect: '/registration' },
    { path: '/registration', name: 'registrationIndex', component: RegistrationIndex },
    { path: '/registration/new', name: 'registrationNew', component: RegistrationNew },
    { path: '/registration/success', name: 'registrationSuccess', component: RegistrationSuccess },
    { path: '*', redirect: '/' }
  ]
})
