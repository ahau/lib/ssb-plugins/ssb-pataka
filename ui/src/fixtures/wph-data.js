// data used in RegistrationNew

export default {
  preferredName: 'Whangaroa Papa Hapū Administrators',
  avatarImage: {
    uri: 'http://whangaroapapahapu.ahau.io:28088/get/%26euxpZPGuqR9Xd6M7ujjfC%2BIfKlPmgRj628%2B65hyeFUI%3D.sha256?unbox=Tc9aKy2Vql2EptIkHINbh8CINkh7KEX0u%2FFPoSymKm0%3D.boxs',
    __typename: 'Image'
  },
  headerImage: null,
  joiningQuestions: [
    {
      label: 'whats your fathers name?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your fathers parents names?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your fathers fathers parents names?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your fathers mothers parents names?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your mothers name?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your mothers parents names?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your mothers fathers parents names?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your mothers mothers parents names?',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'whats your partners name and hapū (if applicable)',
      type: 'input',
      __typename: 'CustomFormField'
    },
    {
      label: 'please provide names, date of birth and genders of any children under 18...',
      type: 'input',
      __typename: 'CustomFormField'
    }
  ],
  customFields: [
    {
      label: 'full name',
      type: 'text',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      key: '1689150525812',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'other names',
      type: 'array',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      key: '1689150526602',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'date of birth',
      type: 'date',
      required: true,
      visibleBy: 'admin',
      tombstone: null,
      key: '1689150529524',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'gender',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      multiple: false,
      options: [
        'male',
        'female',
        'other',
        'unknown'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689150532053'
    },
    {
      label: 'title',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      multiple: true,
      options: [
        'Miss',
        ' Ms',
        ' Mrs',
        ' Mr',
        ' Dr'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689150902426'
    },
    {
      label: 'Please tick if you are whāngai',
      type: 'checkbox',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-15T00:58:50.577Z',
        __typename: 'Tombstone'
      },
      key: '1689150884878',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal information',
      type: 'text',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-12T23:14:02.756Z',
        __typename: 'Tombstone'
      },
      key: '1689150863098',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal address',
      type: 'text',
      required: false,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-15T00:58:47.380Z',
        __typename: 'Tombstone'
      },
      key: '1689150847497',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal city/suburb',
      type: 'text',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-15T00:57:38.208Z',
        __typename: 'Tombstone'
      },
      key: '1689150835716',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal country ',
      type: 'text',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-15T00:57:36.279Z',
        __typename: 'Tombstone'
      },
      key: '1689150814965',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal post code',
      type: 'text',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-15T00:57:34.329Z',
        __typename: 'Tombstone'
      },
      key: '1689150799401',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'Sign up to email distribution',
      type: 'checkbox',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-15T00:57:31.015Z',
        __typename: 'Tombstone'
      },
      key: '1689150783487',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal country',
      type: 'text',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-12T08:32:25.527Z',
        __typename: 'Tombstone'
      },
      key: '1689150659886',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal postcode',
      type: 'text',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-12T08:32:22.234Z',
        __typename: 'Tombstone'
      },
      key: '1689150649755',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'hapū',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-12T08:41:34.729Z',
        __typename: 'Tombstone'
      },
      multiple: true,
      options: [
        'Married a descendent',
        ' Papa Hapū Kaiawhina',
        ' Kai Tangata',
        ' Kaitore',
        ' Mata Rahurahu',
        ' Ngā Uri o Te Pona',
        ' Ngāi Tū Pango',
        ' Ngāti Aukiwa',
        ' Ngāti Haiti',
        ' Ngāti Hao',
        ' Ngāti Imiū',
        ' Ngāti Kahuiti',
        ' Ngāti Kawau',
        ' Ngāti Kawhiti',
        ' Ngāti Kohu',
        ' Ngāti Kura',
        ' Ngāti Miro',
        ' Ngāti Mokokohi',
        ' Ngati Pakahi',
        ' Ngāti Pou',
        ' Ngati Rahurahu',
        ' Ngāti Rangi Matamomoe',
        ' Ngāti Rangi Matakakā',
        ' Ngāti Rihea',
        ' Ngāti Roha',
        ' Ngati Rua',
        ' Ngāti Ruamahue',
        ' Ngāti Takiora',
        ' Ngāti Tara',
        ' Ngāti Tū',
        ' Ngāti Uru',
        ' Ngāti Whakaeke',
        ' Te Aetō',
        ' Te Hoia',
        ' Te Mata Kairiri',
        ' Te Moana',
        ' Te Tahāwai',
        ' Te Teke Tanumea',
        ' Te Uri Pūtete',
        ' Te Uri Kai Whare',
        ' Te Uri o Te Aho',
        ' Te Whānau Pani',
        ' Whanau Pani'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689150926303'
    },
    {
      label: 'hapu',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-15T00:58:53.163Z',
        __typename: 'Tombstone'
      },
      multiple: true,
      options: [
        'Married a descendent',
        ' Papa Hapū Kaiawhina',
        ' Kai Tangata',
        ' Kaitore',
        ' Mata Rahurahu',
        ' Ngā Uri o Te Pona',
        ' Ngāi Tū Pango',
        ' Ngāti Aukiwa',
        ' Ngāti Haiti',
        ' Ngāti Hao',
        ' Ngāti Imiū',
        ' Ngāti Kahuiti',
        ' Ngāti Kawau',
        ' Ngāti Kawhiti',
        ' Ngāti Kohu',
        ' Ngāti Kura',
        ' Ngāti Miro',
        ' Ngāti Mokokohi',
        ' Ngati Pakahi',
        ' Ngāti Pou',
        ' Ngati Rahurahu',
        ' Ngāti Rangi Matamomoe',
        ' Ngāti Rangi Matakakā',
        ' Ngāti Rihea',
        ' Ngāti Roha',
        ' Ngati Rua',
        ' Ngāti Ruamahue',
        ' Ngāti Takiora',
        ' Ngāti Tara',
        ' Ngāti Tū',
        ' Ngāti Uru',
        ' Ngāti Whakaeke',
        ' Te Aetō',
        ' Te Hoia',
        ' Te Mata Kairiri',
        ' Te Moana',
        ' Te Tahāwai',
        ' Te Teke Tanumea',
        ' Te Uri Pūtete',
        ' Te Uri Kai Whare',
        ' Te Uri o Te Aho',
        ' Te Whānau Pani',
        ' Whanau Pani'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689151276336'
    },
    {
      label: 'title',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      multiple: true,
      options: [
        'Miss',
        ' Ms',
        ' Mrs',
        ' Mr',
        ' Dr'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689151312539'
    },
    {
      label: 'description',
      type: 'text',
      required: false,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-12T09:08:37.448Z',
        __typename: 'Tombstone'
      },
      key: '1689152917448',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'birth order',
      type: 'number',
      required: false,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-12T09:08:17.660Z',
        __typename: 'Tombstone'
      },
      key: '1689152897660',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'place of birth',
      type: 'text',
      required: false,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-12T09:08:25.339Z',
        __typename: 'Tombstone'
      },
      key: '1689152905339',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal address is same as above',
      type: 'checkbox',
      required: false,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-15T00:58:59.564Z',
        __typename: 'Tombstone'
      },
      key: '1689201250267',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'status (admin only)',
      type: 'list',
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: '2023-07-14T05:27:28.311Z',
        __typename: 'Tombstone'
      },
      multiple: true,
      options: [
        'registered',
        ' verified',
        ' connected'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689213527440'
    },
    {
      label: 'status',
      type: 'list',
      required: false,
      visibleBy: 'admin',
      tombstone: null,
      multiple: true,
      options: [
        'registered',
        ' verified',
        ' connected'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689312470326'
    },
    {
      label: 'related by',
      type: 'list',
      required: false,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-14T05:28:32.551Z',
        __typename: 'Tombstone'
      },
      multiple: false,
      options: [
        'birth',
        'whangai',
        'adopted'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689312512551'
    },
    {
      label: 'hapū',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: {
        date: '2023-07-15T01:17:01.691Z',
        __typename: 'Tombstone'
      },
      multiple: true,
      options: [
        'Married a descendent',
        ' Papa Hapū Kaiawhina',
        ' Kai Tangata',
        ' Kaitore',
        ' Mata Rahurahu',
        ' Ngā Uri o Te Pona',
        ' Ngāi Tū Pango',
        ' Ngāti Aukiwa',
        ' Ngāti Haiti',
        ' Ngāti Hao',
        ' Ngāti Imiū',
        ' Ngāti Kahuiti',
        ' Ngāti Kawau',
        ' Ngāti Kawhiti',
        ' Ngāti Kohu',
        ' Ngāti Kura',
        ' Ngāti Miro',
        ' Ngāti Mokokohi',
        ' Ngati Pakahi',
        ' Ngāti Pou',
        ' Ngati Rahurahu',
        ' Ngāti Rangi Matamomoe',
        ' Ngāti Rangi Matakakā',
        ' Ngāti Rihea',
        ' Ngāti Roha',
        ' Ngati Rua',
        ' Ngāti Ruamahue',
        ' Ngāti Takiora',
        ' Ngāti Tara',
        ' Ngāti Tū',
        ' Ngāti Uru',
        ' Ngāti Whakaeke',
        ' Te Aetō',
        ' Te Hoia',
        ' Te Mata Kairiri',
        ' Te Moana',
        ' Te Tahāwai',
        ' Te Teke Tanumea',
        ' Te Uri Pūtete',
        ' Te Uri Kai Whare',
        ' Te Uri o Te Aho',
        ' Te Whānau Pani',
        ' Whanau Pani'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689382927234'
    },
    {
      label: 'Please tick if you are whangai',
      type: 'checkbox',
      required: false,
      visibleBy: 'admin',
      tombstone: null,
      key: '1689382911040',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'Tick if your postal address is the same as above',
      type: 'checkbox',
      required: false,
      visibleBy: 'members',
      tombstone: null,
      key: '1689382841414',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'Sign up for email distribution',
      type: 'checkbox',
      required: false,
      visibleBy: 'admin',
      tombstone: null,
      key: '1689382811357',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal city / suburb',
      type: 'text',
      required: false,
      visibleBy: 'members',
      tombstone: null,
      key: '1689382784034',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal country',
      type: 'text',
      required: false,
      visibleBy: 'members',
      tombstone: null,
      key: '1689382770116',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'postal postcode',
      type: 'text',
      required: false,
      visibleBy: 'members',
      tombstone: null,
      key: '1689382757238',
      __typename: 'CommunityCustomField'
    },
    {
      label: 'title',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      multiple: true,
      options: [
        'Miss',
        ' Ms',
        ' Mrs',
        ' Mr',
        ' Dr'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689382961671'
    },
    {
      label: 'hapu',
      type: 'list',
      required: true,
      visibleBy: 'members',
      tombstone: null,
      multiple: true,
      options: [
        'Married a descendent',
        ' Papa Hapū Kaiawhina',
        ' Kai Tangata',
        ' Kaitore',
        ' Mata Rahurahu',
        ' Ngā Uri o Te Pona',
        ' Ngāi Tū Pango',
        ' Ngāti Aukiwa',
        ' Ngāti Haiti',
        ' Ngāti Hao',
        ' Ngāti Imiū',
        ' Ngāti Kahuiti',
        ' Ngāti Kawau',
        ' Ngāti Kawhiti',
        ' Ngāti Kohu',
        ' Ngāti Kura',
        ' Ngāti Miro',
        ' Ngāti Mokokohi',
        ' Ngati Pakahi',
        ' Ngāti Pou',
        ' Ngati Rahurahu',
        ' Ngāti Rangi Matamomoe',
        ' Ngāti Rangi Matakakā',
        ' Ngāti Rihea',
        ' Ngāti Roha',
        ' Ngati Rua',
        ' Ngāti Ruamahue',
        ' Ngāti Takiora',
        ' Ngāti Tara',
        ' Ngāti Tū',
        ' Ngāti Uru',
        ' Ngāti Whakaeke',
        ' Te Aetō',
        ' Te Hoia',
        ' Te Mata Kairiri',
        ' Te Moana',
        ' Te Tahāwai',
        ' Te Teke Tanumea',
        ' Te Uri Pūtete',
        ' Te Uri Kai Whare',
        ' Te Uri o Te Aho',
        ' Te Whānau Pani',
        ' Whanau Pani'
      ],
      __typename: 'CommunityCustomFieldList',
      key: '1689383835044'
    }
  ],
  __typename: 'Community'
}
