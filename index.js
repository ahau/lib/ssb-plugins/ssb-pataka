module.exports = [
  require('./plugins/graphql'),
  require('./plugins/tribes-lite'), // encryption-only!
  require('./plugins/web-registration'),
  require('./plugins/logging')
]
