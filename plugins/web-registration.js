/* eslint-disable brace-style */
const express = require('express')
const Env = require('ahau-env')
const path = require('path')
const https = require('https')
const httpProxy = require('http-proxy')

module.exports = {
  init (ssb, config) {
    const webRegistration = config?.pataka?.webRegistration
    if (!webRegistration) return

    // limit responses to queries to only some tribes
    const supportedTribes = new Set(webRegistration?.tribes)
    if (supportedTribes.size) {
      // TODO - make it so this effects only the web-reg form, and NOT the
      // main pataka dashboard T_T
      ssb.profile.findByGroupId.hook(function (findByGroupId, args) {
        const [groupId, opts, cb] = args

        if (!supportedTribes.has(groupId)) {
          return cb(null, { public: [], private: [] })
          // HACK this is an output which currently gets a tribe ignored
          // see: @ssb-graphql/profile/src/ssb/query/get-tribes.js
        }

        findByGroupId.call(this, groupId, opts, cb)
      })
    }

    startStaticFileServer(ssb, config)
    startProxyServer(ssb, config)
  }
}

function startStaticFileServer (ssb, config) {
  if (!Env().isProduction) return
  // In development, we assume web-registration is run by dev-server

  const app = express()
  const staticFilesPort = config.pataka.webRegistration.port || 8000

  app.use(express.static(path.join(__dirname, '../ui/dist')))
  app.listen(staticFilesPort)

  ssb.close.hook((close, args) => {
    app.close()
    close(...args)
  })
}

function startProxyServer (ssb, config) {
  const proxy = httpProxy.createProxyServer({})

  const { port, httpsProxyPort, https: httpsConfig } = config.pataka.webRegistration

  const opts = {
    key: httpsConfig.privkey,
    cert: httpsConfig.fullchain
  }

  const server = https.createServer(opts, (req, res) => {
    if (req.url === '/graphql') {
      proxy.web(req, res, {
        target: `http://127.0.0.1:${config.graphql.port}`
      })
    }
    else if (req.url.startsWith('/get/%26')) {
      proxy.web(req, res, {
        target: `http://127.0.0.1:${config.serveBlobs.port}`
      })
    }
    else {
      proxy.web(req, res, {
        target: `http://127.0.0.1:${port || 8000}`
      })
    }
  })
  server.listen(httpsProxyPort || 7000)

  ssb.close.hook((close, args) => {
    server.close()
    close(...args)
  })
}
