const ahauServer = require('ahau-graphql-server')
const { promisify } = require('util')

const { pataka: env } = require('ahau-env')()

const Main = require('@ssb-graphql/main')
const Profile = require('@ssb-graphql/profile')
const Artefact = require('@ssb-graphql/artefact')
const Story = require('@ssb-graphql/story')
const Whakapapa = require('@ssb-graphql/whakapapa')
const Submissions = require('@ssb-graphql/submissions')
const Invite = require('@ssb-graphql/invite')
const Pataka = require('@ssb-graphql/pataka')
const Stats = require('@ssb-graphql/stats')

module.exports = {
  async init (ssb, config) {
    const PORT = config?.graphql?.port || env.graphql.port

    const main = Main(ssb, { type: 'pataka' })
    const profile = Profile(ssb)
    const artefact = Artefact(ssb)
    const story = Story(ssb)
    const whakapapa = Whakapapa(ssb, {
      ...profile.gettersWithCache,
      ...story.gettersWithCache,
      ...artefact.gettersWithCache
    })
    const submissions = Submissions(ssb, {
      ...profile.gettersWithCache
    })
    const invite = Invite(ssb)
    const pataka = Pataka(ssb, {
      ...profile.gettersWithCache
    })
    const stats = Stats(ssb)

    let context
    if (!ssb.config.graphql || ssb.config.graphql.loadContext !== false) {
      context = await promisify(main.loadContext)()
    }

    const httpServer = await ahauServer({
      schemas: [
        main,
        profile,
        artefact,
        story,
        whakapapa,
        submissions,
        invite,
        pataka,
        stats
      ],
      context,
      port: PORT,
      allowedOrigins: config.pataka && config.pataka.allowedOrigins
    }).catch(err => err)

    if (httpServer instanceof Error) throw httpServer

    ssb.close.hook((close, args) => {
      httpServer.close()
      close(...args)
    })

    return httpServer
  }
}
