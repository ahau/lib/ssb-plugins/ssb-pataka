const pull = require('pull-stream')
const env = require('ahau-env')

module.exports = {
  init (ssb, config) {
    const logging = !env.isProduction || process.env.AHAU_LOGGING
    if (logging) {
      logPublish(ssb)
      logReplication(ssb)

      // console.log('Logging log:info ...')
      // ssb.on('log:info', m => console.log(m))

      // TODO copy the pataka-cli style into pataka app startup
      // console.log(`| App path | ${config.path}`)
      // console.log(`| SSB port | ${config.port}`)
      // console.log(`| GraphQL  | http://localhost:${port}/graphql`)

      // console.log(
      //   `Graphql Subscriptions ready at ws://localhost:${port}${apollo.subscriptionsPath}`
      // )
    }

    function logPublish (ssb) {
      pull(
        ssb.createUserStream({ id: ssb.id, live: true, old: false, private: true, meta: true }),
        pull.drain(m => {
          console.log('')
          console.log(m.value.sequence, m.key)
          console.log(JSON.stringify(m.value.content, null, 2))
          console.log('')
        })
      )
    }

    function logReplication (ssb) {
      pull(
        ssb.createLogStream({ live: true, old: false }),
        pull.filter(m => m.value.author !== ssb.id),
        pull.drain(m => {
          console.log(`replicated ${m.value.author}, seq: ${m.value.sequence}`)
        })
      )
    }
  }
}
