const Envelope = require('ssb-tribes/envelope')
const KeyRing = require('ssb-keyring')
const bfe = require('ssb-bfe')
const Obz = require('obz')
const { join } = require('path')

// we want our pataka to be able to encrypt messages, but not decrypt because:
// - pataka should never be able to read anything encrypted
// - decryption attempts cost processing time/resource

module.exports = {
  init (ssb, config) {
    /* copied from ssb-tribes/index.js */

    const state = {
      keys: ssb.keys,
      feedId: bfe.encode(ssb.id),

      loading: {
        keystore: Obz()
      },
      newAuthorListeners: [],

      closed: false
    }

    /* secret keys store / helper */
    const keystore = {} // HACK we create an Object so we have a reference to merge into
    KeyRing(join(config.path, 'tribes/keystore'), ssb.keys, (err, api) => {
      if (err) throw err
      Object.assign(keystore, api) // merging into existing reference
      state.loading.keystore.set(false)
    })
    ssb.close.hook(function (close, args) {
      const next = () => close.apply(this, args)
      onKeystoreReady(() => keystore.close(next))
      state.closed = true // NOTE must be after onKeystoreReady call
    })

    /* register the boxer (AND NOT unboxer!) */
    const { boxer } = Envelope(keystore, state)
    ssb.addBoxer({ init: onKeystoreReady, value: boxer })

    function onKeystoreReady (done) {
      if (state.closed === true) return
      if (state.loading.keystore.value === false) return done()

      state.loading.keystore.once(done)
    }
  }
}
