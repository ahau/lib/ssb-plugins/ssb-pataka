const Stack = require('secret-stack')
const env = require('ahau-env')()
const merge = require('lodash.merge')
const chalk = require('chalk')
const boxen = require('boxen')

const plugins = require('./ssb.plugins')
const config = require('./ssb.config')()

const stack = Stack({ caps: env.caps })
  .use(plugins)

const ssb = stack(merge(config, {
  pataka: {
    allowedOrigins: [
      'http://localhost:8087'
      // 'https://dev.pataka.ahau.io' // example
    ]
  }
}))

printConfig(ssb.config)

ssb.invite.create({ uses: 100 }, (err, code) => {
  if (err) console.log('error making invite code', err)
  else console.log('\nPataka Invite:\n', code)
})

function printConfig (config) {
  const envName = env.isProduction ? '' : ` ${env.name.toUpperCase()} `

  const configTxt = chalk`{blue AHAU} {white.bgRed ${envName}}

{bold feedId}  ${config.keys.id}
{bold path}    ${config.path}
{bold network}
  ├── host  ${config.host}
  ├── port  ${config.port}
  └── api   http://localhost:${config.graphql.port}/graphql
`

  // eslint-disable-next-line no-console
  console.log(boxen(configTxt, {
    padding: 1,
    margin: 1,
    borderStyle: 'round',
    borderColor: 'blue'
  }))
}
