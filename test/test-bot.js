const Server = require('scuttle-testbot')

module.exports = function Testbot (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   recpsGuard: Boolean,
  //   keys: SecretKeys
  // }

  if (!opts.serveBlobs) opts.serveBlobs = {}
  if (!opts.serveBlobs.port) opts.serveBlobs.port = 54321

  var stack = Server // eslint-disable-line
    /* @ssb-graphql/main deps */
    .use(require('ssb-blobs'))
    .use(require('ssb-serve-blobs'))

    .use(require('ssb-conn'))
    .use(require('ssb-lan'))
    .use(require('ssb-replicate'))
    .use(require('ssb-friends'))
    .use(require('ssb-invite'))

    /* @ssb-graphql/profile deps */
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('ssb-artefact'))
    .use(require('ssb-story'))
    .use(require('ssb-whakapapa'))
    .use(require('ssb-submissions'))
    .use(require('ssb-settings'))

    .use(require('ssb-recps-guard'))
    .use(require('../'))

  const ssb = stack({
    db1: true,
    ...opts
  })

  ssb.submissions.registerHandler(ssb.profile.person.group)

  return ssb
}
