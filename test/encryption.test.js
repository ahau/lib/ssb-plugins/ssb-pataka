const test = require('tape')
const { promisify: p } = require('util')
const Testbot = require('scuttle-testbot')
const { replicate } = require('scuttle-testbot')
const Pataka = require('./test-bot')

test('encryption', async t => {
  const pataka = Pataka({ name: 'pataka' })

  const kaitiaki = Testbot // eslint-disable-line
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-tribes'))
    .call(null, {
      db1: true,
      name: 'kaitiaki',
      caps: pataka.config.caps
    })

  t.deepEqual(pataka.config.caps, kaitiaki.config.caps, 'same caps')

  const { poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  const content = {
    type: 'one-way-registration',
    recps: [poBoxId]
  }
  const { key } = await p(pataka.publish)(content)

  let msgVal = await p(pataka.get)({ id: key, private: true })
    .catch(err => console.error('WHAT', err))
  t.equal(
    typeof msgVal.content,
    'string',
    'pataka can no longer read message'
  )

  await replicate({ from: pataka, to: kaitiaki })

  msgVal = await p(kaitiaki.get)({ id: key, private: true })
  t.deepEqual(
    msgVal.content,
    content,
    'kaitiaki can read message'
  )

  await p(setTimeout)(100)

  await Promise.all([
    p(pataka.close)(true),
    p(kaitiaki.close)(true)
  ])

  t.end()
})
